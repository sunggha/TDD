# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from django.http import HttpRequest
# from .views import index,validate_npm,delete_friend,friend_list
# from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# from .models import Friend
# from .api_csui_helper.csui_helper import CSUIhelper
# # Create your tests here.
# class Lab7UnitTest(TestCase):
#
#     def test_lab_7_url_is_exist(self):
#         response = Client().get('/lab-7/')
#         self.assertEqual(response.status_code, 200)
#
#     def test_lab_7_using_index_func(self):
#         found = resolve('/lab-7/')
#         self.assertEqual(found.func, index)
#
#     def test_daftar_teman_url_exist(self):
#         response = Client().get('/lab-7/friend-list/')
#         self.assertEqual(response.status_code, 200)
#
#     def test_daftar_teman_using_friend_list_func(self):
#         found = resolve('/lab-7/friend-list/')
#         self.assertEqual(found.func, friend_list)
#
#     def test_lab7_add_friend_success(self):
#         nama = 'Dummy'
#         npm = '09877654321'
#         data = {'name': nama, 'npm': npm}
#         response_post = Client().post('/lab-7/add-friend/', data)
#         self.assertEqual(response_post.status_code, 200)
#
#     def test_delete_friend_success(self):
#         new_friend = Friend.objects.create(friend_name='Dummy', npm='09877654321')
#         response = Client().post('/lab-7/delete-friend/', {'id':new_friend.id})
#         self.assertEqual(response.status_code, 200)
#
#     def test_validate_npm(self):
#         new_friend = Friend.objects.create(friend_name='Dummy', npm='09877654321')
#         response = Client().post('/lab-7/validate-npm/', {'npm': new_friend.npm})
#         self.assertEqual(dict, type(response.json()))
#
#     def test_get_daftar(self):
#         new_friend = Friend.objects.create(friend_name='Dummy', npm='09877654321')
#         response = Client().post('/lab-7/get-friend-list/')
#         self.assertEqual(dict, type(response.json()))
#
#     def test_auth_param_dict(self):
#         csui_helper = CSUIhelper()
#         auth_param = csui_helper.instance.get_auth_param_dict()
#         self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])
#
#     def test_invalid_sso_raise_exception(self):
#         username = "dummy"
#         password = "dummy"
#         csui_helper = CSUIhelper()
#         with self.assertRaises(Exception) as context:
#             csui_helper.instance.get_access_token(username, password)
#             self.assertIn("dummy", str(context.exception))
#
#
